// import module
var express = require('express');
var TodoList = require('todo');
var router = express.Router();

// create todo list object
var todoList = new TodoList();

router.get('/', function(req, res, next) {
    // render assignment page
    res.render('index',{ title : 'Todo-list API'});
});

router.get('/list', function(req, res, next) {
    // get & respone all task in list
    res.send(todoList.getAll());
});

router.route('/list/:index')
    .get(function(req, res, next) {
        // set data
        var index = parseInt(req.params.index);

        // get & respone a task at element [index]
        res.send(todoList.getByIndex(index));
    })
    .put(function(req, res, next) {
        // set data
        var index = parseInt(req.params.index);

        // update subject of a task at element [index]
        todoList.editSubjectAt(index, req.body.subject);

        // update detail of a task at element [index]
        todoList.editDetailAt(index, req.body.detail);

        // update status of a task at element [index]
        todoList.setStatusAt(index, req.body.status);

        // get & respone a task of index in list (after modify)
        res.send(todoList.getByIndex(index));
    })
    .delete(function(req, res, next) {
        // set data
        var index = parseInt(req.params.index);

        // delete
        todoList.deleteAt(index);

        // get & respone all task in list (after modify)
        res.send(todoList.getAll());
    });

router.post('/add', function(req, res, next) {
    // create & add task to list
    todoList.add(req.body.subject, req.body.detail);

    // get & respone lastest that was added
    res.send(todoList.getLastest());
});

router.delete('/clear', function(req, res, next) {
    res.send(todoList.clear());
});

module.exports = router;
