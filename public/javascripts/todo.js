initList();

function viewTasks() {
    // show document
    showDocument(
    	'View todo-list',
    	'view all items in the list',
    	'GET /list',
        'None',
        'None',
        JSON.stringify(
            [{
                "subject": "My first task",
                "detail": "Todo list",
                "status": "pending"
            }, {
                "subject": "My first task",
                "detail": "Todo list",
                "status": "pending"
            }], undefined, 2),
        'None',
        '$.get( "/list", function( data ) {console.log(data);});');

    // show example result
    $.get("/list", function(result) {
        showResult(result);
        // set message result
        $("#message").text("Get all task done !");
    });
}

function viewATask() {
    // show document
    showDocument(
		'View todo-task',
    	'view a single task in the list',
    	'GET /list/:index',
        'index = [integer]',
        'None',
        JSON.stringify({
            "subject": "Task number 0",
            "detail": "I have some thing to do now !",
            "status": "pending"
        }, undefined, 2),
        'None',
        '$.get( "/list/0", function( data ) {console.log(data);});');

    // show example result
    $.get("/list/0", function(result) {
        showResult(result);
        // set message result
        $("#message").text("Get task 0 done !");
    });
}

function addTask() {
    // show document
    showDocument(
    	'Add todo-task',
    	'add a task to the list',
    	'POST /list',
        'None',
        'subject = [String] , detail = [String]',
        JSON.stringify({
            "subject": "Task number 1",
            "detail": "I have some thing to do now !",
            "status": "pending"
        }, undefined, 2),
        'None',
        '$.post("/add", { subject: "Task number ", detail: "I have some thing to do now !"}).done(function(result) {console.log(result);});');

    // show example result
    $.get("/list", function(todoList) {
        // add task
        $.post("/add", {
            subject: "Task number " + todoList.length.toString(),
            detail: "I have some thing to do now !"
        }).done(function(result) {
            showResult(result);
            // set message result
            $("#message").text("Task " + todoList.length.toString() + " was added !");
        });
    });
}

function editTask() {
    // show document
    showDocument(
    	'Edit todo-task',
    	'edit existing task',
    	'PUT /list/:index',
        'index = [integer]',
        'subject = [String] , detail = [String] , status = [String]',
        JSON.stringify({
            "subject": "Task number 1",
            "detail": "Do it now !",
            "status": "pending"
        }, undefined, 2),
        'None',
        '$.ajax({ url: "/list/0", type: "PUT", data: { detail: "Do it now !" }, success: function(result) { console.log(result); } });');


    // show example result
    $.ajax({
        url: '/list/0',
        type: 'PUT',
        data: {
            detail: "Do it now !"
        },
        success: function(result) {
            showResult(result);
            // set message result
            $("#message").text("Task 0 was edited !");
        }
    });
}

function setStatus() {
    // show document
    showDocument(
    	'Set todo-task status',
    	'set the task status',
    	'PUT /list/:index',
        'index = [integer]',
        'subject = [String] , detail = [String] , status = [String]',
        JSON.stringify({
            "subject": "Task number 1",
            "detail": "I have some thing to do now !",
            "status": "done"
        }, undefined, 2),
        'None',
        '$.ajax({ url: "/list/0", type: "PUT", data: { status: "done" }, success: function(result) { console.log(result); } });');

    // show example result
    $.ajax({
        url: '/list/0',
        type: 'PUT',
        data: {
            status: "done"
        },
        success: function(result) {
            showResult(result);
            // set message result
            $("#message").text("Task 0 's status was set to done !");
        }
    });
}

function delTask() {
    // show document
    showDocument(
    	'Delete todo-task',
    	'delete a task from the list',
    	'DELETE /list/:index',
        'index = [integer]',
        'None',
        JSON.stringify(
            [{
                "subject": "My first task",
                "detail": "Todo list",
                "status": "pending"
            }, {
                "subject": "My first task",
                "detail": "Todo list",
                "status": "pending"
            }], undefined, 2),
        'None',
        '$.ajax({ url: "/list/0", type: "DELETE", success: function(result) { console.log(result); } });');


    // show example result
    $.ajax({
        url: '/list/0',
        type: 'DELETE',
        success: function(result) {
            showResult(result);
            // set message result
            $("#message").text("Task 0 was deleted !");
        }
    });
}

function clearList() {
    // show document

    // show example result
    $.ajax({
        url: '/clear',
        type: 'DELETE',
        success: function(result) {
            showResult(result);
            // set message result
            $("#message").text("All clear !");
        }
    });
}

function initList() {
    // reset list
    clearList();

    // add data
    addTask();

    // first api document
    viewTasks();
}

function showResult(data) {
    document.getElementById("result").innerHTML = JSON.stringify(data, undefined, 2);
}

function showDocument(name,detail,url, urlParams, dataParams, content, error, sample) {
    $("#api-name").text(name);
    $("#api-detail").text(detail);
    $("#api-url").text(url);
    $("#api-url-params").text(urlParams);
    $("#api-data-params").text(dataParams);
    $("#api-response-200").text(content);
    $("#api-response-err").text(error);
    $("#api-sample").text(sample);
}
